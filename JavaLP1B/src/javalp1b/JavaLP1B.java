/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javalp1b;

import java.util.Scanner;

/**
 *
 * @author edgar
 */
public class JavaLP1B {

    public static void texto(String sMensaje){
        System.out.println(sMensaje);
    }

    public static void separador(){
        texto("-------------------------------------");
    }
    
    
    public static void menu(){
        texto("1.- Levantarse");
        texto("2.- Desayunar");
        texto("3.- Unirse al Meet");
        texto("4.- Estar atento");
        texto("5.- Enviar la tarea");
        texto("6.- Ejemplos For");
    }
    
    public static void opciones_menu(int opcion){
        if (opcion == 1){
            texto("6:45, para clases de LP1");
        }
        if (opcion == 2){
            texto("Café y pan");            
        }
        if (opcion == 3){
             texto("19 de 35");
         }
         if (opcion == 4){
            texto("3 respondiendo");
        }
        if (opcion == 5){
            texto("Espero que todos");
        }
    }    
    
     public static void opciones_switch_String(String opcion){
        switch (opcion){
            case "uno":
                texto("Opcion1");
                break;
            case "dos":
                texto("Opcion2");
                break;
            case "tres":
                texto("Opcion3");
                break;
            default:
                texto("Opcion No valida");
                break;
            
        }
     }
     
    public static void estructura_For(){
        texto("Recorrido de arreglo tipo Int");
        int[] numeros = {3,4,5,6,7,8,9,10,0,1,2};
        for (int i : numeros) {
             System.out.println("valor: " + i);
        }
        
        texto("Recorrido de arreglo tipo String");
        String[] cadena = {"uno", "dos", "tres"};
        for (String i : cadena) {
             System.out.println("cadena: " + i);
        }
        
        texto("Recorrido For tradicional con decrementos");
        System.out.println("for i");
        for (int i = 100; i >= 10; i-=5) {
            System.out.println( i);
        }

        texto("Recorrido For tradicional con inrementos");
        System.out.println("for i");
        for (int i = 0; i <= 10; i++) {
            System.out.println( i);
        }          
    }
    
    
    public static void opciones_switch(int opcion){
        switch (opcion){
            case 1:
                texto("6:45, para clases de LP1");
                opciones_switch_String("uno");
                break;
            case 2:
                texto("Café y pan");
                opciones_switch_String("dos");
                break;
            case 3:
                texto("1er Trimestre del Año");
                opciones_switch_String("tres");
                break;
            case 4:
                texto("3 respondiendo");
                opciones_switch_String("cinco");
                break;
            case 5:
                texto("Espero que todos");
                opciones_switch_String("diez");
                break;
            case 6:
                texto("Ejemplos de Estructura de COntrol For");
                estructura_For();
                break;
            default:
                texto("Opcion No valida");
                texto("Teclee un valor dentro de rango ");
                break;    
        }        
   }    
   
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        texto("Programa con uso de la Estructura IF Simple");
        separador();
        menu();
        Scanner entrada = new Scanner( System.in );
        System.out.print("Leer opción : ");        
        int iValor1 = entrada.nextInt();
        separador();
//        opciones_menu(iValor1);
        opciones_switch(iValor1);
       
        
   
        
    }    
    
}
