/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javalp1b;

/**
 *
 * @author edgar
 */
public class Universidad {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Persona alumno61778 = new Persona(61778, "Pedro Pablo", 20, "Alumno" );
        Persona alumno63178 = new Persona(63178, "Jo Luis", 22, "Alumno" );
        Persona alumno63758 = new Persona(63758, "Maria", 31, "Alumno" );
        Persona alumno63768 = new Persona(63768, "Susana", 25, "Alumno" );
        
        
        System.out.println("----------------------Alumnos ------------------------------");
        
        
        Persona alumno63778 = new Persona(63778, "Juan Pedro", 21, "Alumno" );
        System.out.println(alumno63778.toString());
        alumno63778.setNombre("Pedro Pablo");
        alumno63778.setEdad(22);
        System.out.println(alumno63778.getId());
        System.out.println(alumno63778.getNombre());
        
        System.out.println(alumno63778.toString());
        
        
        
        System.out.println(alumno61778.toString());
        System.out.println(alumno63178.toString());
        System.out.println(alumno63758.toString());
        System.out.println(alumno63768.toString());
        
        Persona docent63778 = new Persona(63778, "Juan Pedro", 41, "Docente" );
        Persona docent61778 = new Persona(61778, "Pedro Pablo", 40, "Docente" );
        Persona docent63178 = new Persona(63178, "Jo Luis", 42, "Docente" );
        Persona docent63758 = new Persona(63758, "Maria", 41, "Docente" );
        Persona docent63768 = new Persona(63768, "Susana", 45, "Docente" );
        
        System.out.println("----------------------Docentes ------------------------------");
        System.out.println(docent63778.toString());
        System.out.println(docent61778.toString());
        System.out.println(docent63178.toString());
        System.out.println(docent63758.toString());
        System.out.println(docent63768.toString());
        
    }
    
}
