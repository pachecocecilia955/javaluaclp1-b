/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javalp1b;

import java.util.Scanner;

/**
 *
 * @author edgar
 */
public class CiclosWhileDoWhile {

    public static void estructura_While(){
        // Uso Ciclico incremento
        int x = 1;
        while (x <= 10)
        {
            System.out.println("Valor de x: " + x);
            x++;
        }
        x = 1;
        while (x <= 10)
        {
            System.out.println("Valor de x: " + x);
            x+=2;
        }
         
        // Uso Ciclico Decremento
        x = 20;
        while (x >= 10)
        {
            System.out.println("Valor de x: " + x);
            x--;
        }
        
        boolean valor = true;
        x = 1;
        while (valor)
        {
            System.out.println("Valor de x: " + x);
            x++;
            if (x == 30){
                valor = false;
            }
        }
        valor = true;
        x = 1;
        int suma = 0;
        while (valor)
        {
            System.out.println("Valor de x: " + x+ "  = "+suma );
             x++;
             suma += x;  // Comentar suma
            if (suma >= 30){
                valor = false;
                
            }
        }        
    }
    
    public static void estructura_DoWhile(){
        int x = 1;
        
        do {
            System.out.println("Valor de x: " + x);
            x++;
        } while (x <= 10);
        
        x = 1;
        
        do {
            System.out.println("Valor de x: " + x);
            x+=2;
        } while (x <= 10);
         
        // Uso Ciclico Decremento
        x = 20;
        
        do {
            System.out.println("Valor de x: " + x);
            x--;
        }while (x >= 10);
        
        boolean valor = true;
        x = 1;
        
        do {
            System.out.println("Valor de x: " + x);
            x++;
            if (x == 30){
                valor = false;
            }
        }while (valor);
        valor = true;
        x = 1;
        int suma = 0;
        
        do {
            System.out.println("Valor de x: " + x+ "  = "+suma );
             x++;
             suma += x;  // Comentar suma
            if (suma >= 30){
                valor = false;
                
            }
        } while (valor);
        
    }
    static void imprimirMensaje(String sMensaje){
        System.out.println(sMensaje);
    }
    
 
    static void menu(){
        imprimirMensaje("1.- Comparar dos Números ");
        imprimirMensaje("2.- Calcular el área de un Triangulo ");
        imprimirMensaje("3.- Calcular el área de un Cuadrado");
        imprimirMensaje("4.- Calcular el área de un Circulo");
        imprimirMensaje("5.- Calcular el área del Pentagono");
        imprimirMensaje("9.- Salir");
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        
        // estructura_While();
        // estructura_DoWhile();
        int opcionmenu; 
        do {
            menu();
           Scanner entrada = new Scanner( System.in );
           imprimirMensaje("Teccle la opción deseada __ ");        
           opcionmenu = entrada.nextInt();
           //submenu(opcionmenu);
        } while (opcionmenu != 9);        
        
        
        
    }
    
}
